import java.util.*;

/*
	8 Queen problme

*/
public class main{

	public static void main(String[] args) {
		int sizeOfTheBoard = 8;

		int[][] board = new int[sizeOfTheBoard][sizeOfTheBoard];

		int[][] queens = new int[sizeOfTheBoard][sizeOfTheBoard];

		String[] test = new String[8];

		// Initial queen position
		//Scanner scanner = new Scanner(System.in);
		//System.out.println("Enter first queen position");
		//int[] initialInput = convertLetterToNumber(scanner.nextLine());

		int[] initialInput = convertLetterToNumber("5b");


		// Init board with some values
		for(int rows = 0; rows < sizeOfTheBoard; rows++){
			for(int col = 0; col < sizeOfTheBoard; col++){
				board[rows][col] = 0;

				System.out.print(board[rows][col]);
			}
			System.out.println();
		}

		queens[initialInput[0]][initialInput[1]] = 1;


		System.out.println("\n");

		int[][] collisionList = new int[sizeOfTheBoard][sizeOfTheBoard];
		collisionList = queenPossibleMoves(initialInput[0], initialInput[1], sizeOfTheBoard, board);


		for(int rows = 0; rows < sizeOfTheBoard; rows++){
			for(int col = 0; col < sizeOfTheBoard; col++){

				if(collisionList[rows][col] == 2) {
					//System.out.println("COLISSION");
				}
				else{
					queens[rows][col] = 1;
					test[rows] = rows + " " + col;

					System.out.println(test[rows]);

					//collisionList = queenPossibleMoves(rows, col, sizeOfTheBoard, collisionList);			
				}
			}
		}


	//	printBoard(collisionList, sizeOfTheBoard);
		printBoard(board, sizeOfTheBoard);


	}

	// Converts input (initial queen coordinates) to numerical coordinates
	// Example: 1C would be 13
	public static int[] convertLetterToNumber(String input){
		String temp = String.valueOf(input.charAt(1));
		// Whole alphabet incase board size would change
		String alphabet = "abcdefghijklmnopqrstuvwxyz";

   		int output = alphabet.indexOf(temp);
		
		int[] result = new int[2];
		result[0] = Integer.parseInt(String.valueOf(input.charAt(0))) - 1;
		result[1] = output;
		return result;
	}

	// Prints the board
	public static void printBoard(int[][] board, int sizeOfTheBoard) {
		for(int rows = 0; rows < sizeOfTheBoard; rows++){
			for(int col = 0; col < sizeOfTheBoard; col++){
				System.out.print(board[rows][col] + " ");

			}
			System.out.println();
		}
		System.out.println("\n");
	}

	public static boolean isSafe(int rows, int col){

		// Check vertical / horizontal
		// for example queen coordinate is 33 (3C)


		return false;
	}

	public static int[][] queenPossibleMoves(int rows, int col, int sizeOfTheBoard, int[][] collisionList) {
		// 2 means collision, 0 means safe
		for (int i = 0; i < sizeOfTheBoard; i++) { // rows
			for(int j = 0; j < sizeOfTheBoard; j++) { // 
				// check vertical and horizontal rows
				if(i == rows || j == col) {
					collisionList[i][j] = 2;
				}
			}
		}

		// check upper left diagonal
		for (int i = rows, j = col; i >= 0 && j >= 0; i--, j--){
			collisionList[i][j] = 2;
		}
		// check lower left diagonal
		for (int i = rows, j = col; j >= 0 && i < sizeOfTheBoard; i++, j--) {
			collisionList[i][j] = 2;
		}
		// check lower right diagonal
		for (int i = rows, j = col; i < sizeOfTheBoard && j < sizeOfTheBoard; i++, j++){
			collisionList[i][j] = 2;
		}

		// check upper right diagonal
		for (int i = rows, j = col; j < sizeOfTheBoard && i >= 0; i--, j++) {
			collisionList[i][j] = 2;
		}
            
		return collisionList;
	}

}







